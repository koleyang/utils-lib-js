export * from "./object"
export * from "./base"
export * from "./array"
export * from "./function"
export * from "./element"
export * from "./static"
export * from "./types"
export * from "./event"
export * from "./storage"
export * from "./log"
export * from "./animate"
export * from "event-message-center"
export * from "task-queue-lib"
export * from "js-request-lib"
export * from "timer-manager-lib"
export * from "js-log-lib"

import object from "./object"
import base from "./base"
import array from "./array"
import __function from "./function"
import element from "./element"
import __static from "./static"
import event from "./event"
import storage from "./storage"
import log from "./log"
import animate from "./animate"
import eventMessageCenter from "event-message-center"
import taskQueueLib from "task-queue-lib"
import JSRequest from "js-request-lib"
import TimerManager from "timer-manager-lib"
import JSLogLib from "js-log-lib"
export default {
    ...object,
    ...base,
    ...array,
    ...__function,
    ...element,
    ...__static,
    ...event,
    ...storage,
    ...log,
    ...animate,
    eventMessageCenter,
    taskQueueLib,
    JSRequest,
    TimerManager,
    JSLogLib
}