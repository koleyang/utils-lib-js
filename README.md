# utils-lib-js

## 介绍

JavaScript 工具函数，封装的一些常用的 js 函数

## 调试说明

1.  pnpm build（构建）
2.  pnpm debug（调试源码）
3.  在 esm，cjs，window 环境下导入该工具库

## 参与贡献

1.  Fork 本仓库
2.  Star 仓库
3.  提交代码
4.  新建 Pull Request

## 使用说明

### 安装

`npm install utils-lib-js`
或
`yarn add utils-lib-js`
或
`pnpm install utils-lib-js`

### 引入

#### ESM

```javascript
import { defer, messageCenter, TaskQueue, Request } from "utils-lib-js";
```

#### CJS

```javascript
const { defer, messageCenter, TaskQueue } = require("utils-lib-js");
```

#### 浏览器中

```html
<script src="./node_modules/utils-lib-js/dist/umd/index.js"></script>
<script>
  console.log(UtilsLib);
</script>
```

### TS 接口注释

参照 https://gitee.com/DieHunter/utils-lib-js/blob/master/src/types.ts

### 代码演示

#### base 模块

##### 1. `randomNum(min: number, max: number, bool?: boolean): number`

生成指定范围内的随机整数。

- `min`: 最小值（包含）。
- `max`: 最大值（包含）。
- `bool`: 是否包含最大值，默认为 `false`。

```javascript
const randomNumber = randomNum(1, 10);
console.log(randomNumber); // 生成一个介于 1 到 10 之间的随机整数
```

##### 2. `urlSplit(url: string): object`

将 URL 查询参数解析为对象。

- `url`: 包含查询参数的 URL。

```javascript
const url = "https://example.com/path?param1=value1&param2=value2";
const params = urlSplit(url);
console.log(params);
// 输出: { param1: 'value1', param2: 'value2' }
```

##### 3. `urlJoin(url: string, query: object): string`

将对象转换为 URL 查询参数，并与原始 URL 连接。

- `url`: 原始 URL。
- `query`: 包含查询参数的对象。

```javascript
const url = "https://example.com/path";
const query = { param1: "value1", param2: "value2" };
const newUrl = urlJoin(url, query);
console.log(newUrl);
// 输出: https://example.com/path?param1=value1&param2=value2
```

##### 4. `getType(data: any): string`

获取数据的类型。

- `data`: 要检查的数据。

```javascript
const dataType = getType("Hello, World!");
console.log(dataType); // 输出: string
```

##### 5. `getTypeByList(data: any, whiteList: string[]): boolean`

检查数据类型是否在白名单内。

- `data`: 要检查的数据。
- `whiteList`: 允许的数据类型列表。

```javascript
const data = 42;
const allowedTypes = ["number", "string"];
const isTypeAllowed = getTypeByList(data, allowedTypes);
console.log(isTypeAllowed); // 输出: true
```

##### 6. `toKebabCase(camelCase: string, separator: string = "-"): string`

驼峰式命名转换为连字符式命名

- `camelCase`: 驼峰式变量
- `separator`: 分隔符

```javascript
const camelCase = "fontSize";
const kebabCase = toKebabCase(camelCase);
console.log(kebabCase); // 输出: font-size
```

#### object 模块

##### 1. `getValue(object: object, key: string, defaultValue: any = ''): any`

获取对象中指定路径的值。

- `object`: 目标对象。
- `key`: 要获取的值的路径。
- `defaultValue`: 默认值，当路径不存在时返回。

```javascript
const obj = { a: { b: { c: 42 } } };
const value = getValue(obj, "a.b.c", "default");
console.log(value); // 输出: 42
```

##### 2. `setValue(object: object, key: string, value: any = {}): object`

设置对象中指定路径的值。

- `object`: 目标对象。
- `key`: 要设置的值的路径。
- `value`: 要设置的值。

```javascript
const obj = { a: { b: { c: 42 } } };
setValue(obj, "a.b.d", "new value");
console.log(obj); // 输出: { a: { b: { c: 42, d: 'new value' } } }
```

##### 3. `mixIn(target: object, source: object = {}, overwrite: boolean = false): object`

将源对象的属性混入目标对象中。

- `target`: 目标对象。
- `source`: 源对象。
- `overwrite`: 是否覆盖已存在的属性，默认为 `false`。

```javascript
const target = { a: 1 };
const source = { b: 2 };
const result = mixIn(target, source);
console.log(result); // 输出: { a: 1, b: 2 }
```

##### 4. `enumInversion(target: object): object`

反转枚举对象的键值对。

- `target`: 要反转的枚举对象。

```javascript
const enumObj = { key1: "value1", key2: "value2" };
const invertedEnum = enumInversion(enumObj);
console.log(invertedEnum); // 输出: { value1: 'key1', value2: 'key2' }
```

##### 5. `isNotObject(source: any, type: string): boolean`

检查值是否为非对象类型。

- `source`: 要检查的值。
- `type`: 数据类型。

```javascript
const result = isNotObject(42, "number");
console.log(result); // 输出: false
```

##### 6. `cloneDeep(target: any): any`

深度克隆对象。

- `target`: 要克隆的对象。

```javascript
const obj = { a: { b: { c: 42 } } };
const clonedObj = cloneDeep(obj);
console.log(clonedObj); // 输出: { a: { b: { c: 42 } } }
```

##### 7. `createObjectVariable(type: string, source: object = {}): object`

根据类型创建特定类型的对象。

- `type`: 要创建的对象类型。
- `source`: 初始化对象的数据。

```javascript
const array = createObjectVariable("array", [1, 2, 3]);
console.log(array); // 输出: [1, 2, 3]
```

##### 8. `createObject(source: object): object`

使用原型链创建对象。

- `source`: 原型对象。

```javascript
const protoObj = { a: 1 };
const newObj = createObject(protoObj);
console.log(newObj.a); // 输出: 1
```

##### 9. `inherit(source: object, target: function): function`

继承原型链。

- `source`: 父对象的原型链。
- `target`: 子对象的构造函数。

```javascript
function Parent() {}
Parent.prototype.method = function () {
  console.log("Hello from parent");
};

function Child() {}
inherit(Parent, Child);
const childInstance = new Child();
childInstance.method(); // 输出: Hello from parent
```

##### 10. `getInstance(classProto: function, overwrite: boolean = false, ...params: any[]): object`

获取类的单例实例。

- `classProto`: 类的原型链。
- `overwrite`: 是否覆盖已存在的实例，默认为 `false`。
- `params`: 类构造函数的参数。

```javascript
class Singleton {
  constructor(name) {
    this.name = name;
  }
}

const instance1 = getInstance(Singleton, false, "Instance 1");
const instance2 = getInstance(Singleton, true, "Instance 2");

console.log(instance1 === instance2); // 输出: false
```

##### 11. `classDecorator(params: object): ClassDecorator`

为类添加装饰器。

- `params`: 要添加的属性和方法。

```javascript
@classDecorator({
  additionalMethod: function () {
    console.log("Additional method");
  },
})
class DecoratedClass {}

const instance = new DecoratedClass();
instance.additionalMethod(); // 输出: Additional method
```

##### 12. `stringToJson(target: string): object | null`

将 JSON 字符串转换为对象。

- `target`: 要转换的 JSON 字符串。

```javascript
const jsonString = '{"key": "value"}';
const jsonObject = stringToJson(jsonString);
console.log(jsonObject); // 输出: { key: 'value' }
```

##### 13. `jsonToString(target: any): string`

将对象转换为 JSON 字符串。

- `target`: 要转换的对象。

```javascript
const obj = { key: "value" };
const jsonString = jsonToString(obj);
console.log(jsonString); // 输出: '{"key":"value"}'
```

##### 14. `isWindow(win: any): boolean`

检查是否是浏览器窗口。

- `win`: 要检查的对象。

```javascript
const isBrowserWindow = isWindow(window);
console.log(isBrowserWindow); // 输出: true
```

##### 14. `emptyObject(init: IObject): object`

创建原型为空的对象。

- `init`: 初始化对象。

```javascript
const o = emptyObject({ name: "hunter" });
const o2 = { name: "hunter" };
console.log(o.__proto__, o2.__proto__); // 输出: undefined, [Object: null prototype] {}
```

##### 15. `isEmptyObject(object: IObject<unknown>): boolean`

判断是否是空对象

- `object`: 目标对象。

```javascript
const o = isEmptyObject({ name: "阿宇" });
const o2 = isEmptyObject({});
console.log(o, o2); // 输出: false true
```

#### array 模块

##### 1. `arrayRandom(arr: any[]): any[]`

对数组进行随机排序。

- `arr`: 要排序的数组。

```javascript
const originalArray = [1, 2, 3, 4, 5];
const randomizedArray = arrayRandom(originalArray);
console.log(randomizedArray);
// 输出: 一个随机排序的数组
```

##### 2. `arrayUniq(arr: any[]): any[]`

从数组中移除重复的元素。

- `arr`: 要处理的数组。

```javascript
const arrayWithDuplicates = [1, 2, 2, 3, 4, 4, 5];
const uniqueArray = arrayUniq(arrayWithDuplicates);
console.log(uniqueArray);
// 输出: 移除重复元素后的数组
```

##### 3. `arrayDemote(arr: IDemoteArray<any>, result: any[] = []): any[]`

将多层嵌套的数组降维。

- `arr`: 要降维的数组。
- `result`: 用于存储结果的数组，默认为空数组。

```javascript
const nestedArray = [1, [2, [3, [4]], 5]];
const demotedArray = arrayDemote(nestedArray);
console.log(demotedArray);
// 输出: 降维后的数组 [1, 2, 3, 4, 5]
```

#### function 模块

##### 1. `throttle(fn: Function, time: number): Function`

限制函数在指定时间内的调用频率。

- `fn`: 要执行的函数。
- `time`: 时间间隔（毫秒）。

```javascript
const throttledFunction = throttle(
  () => console.log("Throttled function called!"),
  1000
);
throttledFunction(); // 只有在 1 秒后才会执行
```

##### 2. `debounce(fn: Function, time: number): Function`

限制函数在指定时间内的连续调用。

- `fn`: 要执行的函数。
- `time`: 时间间隔（毫秒）。

```javascript
const debouncedFunction = debounce(
  () => console.log("Debounced function called!"),
  1000
);
debouncedFunction(); // 在 1 秒内多次调用，只有最后一次会执行
```

##### 3. `defer(timer: number = 0): { promise: Promise<void>, resolve: Function, reject: Function }`

创建一个扁平化的 Promise 延迟对象，可以设置在一定时间后超时。

- `timer`: 超时时间（毫秒），默认为 0。

```javascript
const deferFn = () => {
  const { resolve, promise } = defer();
  setTimeout(() => resolve("success"), 500);
  return promise;
};
const delayFn = () => {
  const { promise } = defer(1000);
  return promise;
};
deferFn().then(console.log); // 延迟输出
delayFn().catch(console.error); // 超时输出
```

##### 4. `catchAwait(defer: Promise<any>): Promise<[Error | null, any]>`

捕获异步操作的错误并返回 `[Error, null]` 或 `[null, result]`。

- `defer`: 异步操作的 Promise 对象。

```javascript
const asyncOperation = new Promise((resolve, reject) => {
  // 模拟异步操作
  setTimeout(() => {
    reject(new Error("Something went wrong!"));
  }, 1000);
});

const [error, result] = await catchAwait(asyncOperation);
console.log(error); // 输出: Error: Something went wrong!
console.log(result); // 输出: null
```

##### 5. `requestFrame(callback: (timestamp: number) => void, delay?: number): () => void`

在浏览器和 Node.js 环境中使用的自定义帧回调函数（类似 setinterval）

- callback: 每一帧中要执行的回调函数，接收一个参数 timestamp 表示当前时间戳。
- delay（可选）: 每次执行回调函数之间的时间间隔（毫秒），默认为 0。

```javascript
let count = 0;
const clear = requestFrame(() => {
  console.log(count);
  count++;
  if (count === 5) {
    clear(); // 取消执行
  }
}, 1000);
```

#### element 模块

##### 1. `createElement(options: { ele?: string | HTMLElement, style?: object, attr?: object, parent?: HTMLElement }): HTMLElement`

创建并返回一个 HTML 元素。

- `ele`: 元素类型或已存在的 HTMLElement 对象，可选，默认为 'div'。
- `style`: 元素的样式对象，可选。
- `attr`: 元素的属性对象，可选。
- `parent`: 元素的父级元素，可选。

```javascript
const options = {
  ele: "div",
  style: { color: "blue", fontSize: "16px" },
  attr: { id: "myElement", className: "custom-class" },
  parent: document.body,
};

const createdElement = createElement(options);
// 在 body 中创建一个带有样式和属性的 div 元素
```

#### event 模块

##### 1. `addHandler(ele: HTMLElement, type: string, handler: EventListener): void`

为元素添加事件监听器。

- `ele`: 目标元素。
- `type`: 事件类型。
- `handler`: 事件处理函数。

```javascript
const button = document.getElementById("myButton");
const handleClick = () => console.log("Button clicked!");
addHandler(button, "click", handleClick);
```

##### 2. `stopBubble(event: Event): void`

阻止事件冒泡。

- `event`: 事件对象。

```javascript
const handleClick = (event) => {
  console.log("Button clicked!");
  stopBubble(event);
};
```

##### 3. `stopDefault(event: Event): void`

阻止事件的默认行为。

- `event`: 事件对象。

```javascript
const handleFormSubmit = (event) => {
  console.log("Form submitted!");
  stopDefault(event);
};
```

##### 4. `removeHandler(ele: HTMLElement, type: string, handler: EventListener): void`

移除元素的事件监听器。

- `ele`: 目标元素。
- `type`: 事件类型。
- `handler`: 要移除的事件处理函数。

```javascript
const button = document.getElementById("myButton");
const handleClick = () => console.log("Button clicked!");
addHandler(button, "click", handleClick);
// 其他操作...
removeHandler(button, "click", handleClick);
```

##### 5. `dispatchEvent(ele: HTMLElement, data: any): void`

触发自定义事件。

- `ele`: 目标元素。
- `data`: 自定义事件的数据。

```javascript
const customEvent = new CustomEvent("customEvent", {
  detail: { key: "value" },
});
const targetElement = document.getElementById("myElement");
dispatchEvent(targetElement, customEvent);
```

#### storage 模块

##### 1. `setStorage(key: string, val: any): void`

将值存储到本地存储中。

- `key`: 存储的键名。
- `val`: 要存储的值。

```javascript
const userData = { username: "john_doe", email: "john@example.com" };
setStorage("user_data", userData);
```

##### 2. `getStorage(key: string): any`

从本地存储中获取值。

- `key`: 要获取的键名。

```javascript
const storedUserData = getStorage("user_data");
console.log(storedUserData);
// 输出: { username: 'john_doe', email: 'john@example.com' }
```

##### 3. `clearStorage(key?: string): void`

清除本地存储中的值。

- `key`: 可选，要清除的键名。如果未提供键名，则清除所有存储。

```javascript
clearStorage("user_data"); // 清除特定键名的存储值
// 或者
clearStorage(); // 清除所有本地存储的值
```

#### log 模块

##### 1. `logOneLine(str: string, overwrite: boolean = false, wrap: boolean = true): void`

在一行中输出日志。

- `str`: 要输出的字符串。
- `overwrite`: 是否覆盖当前行，默认为 `false`。
- `wrap`: 是否在输出后换行，默认为 `true`。

```javascript
logOneLine("This is a single line log message.");
```

##### 2. `logLoop(opts?: { loopList?: string[], isStop?: boolean, timer?: number, index?: number }): { loopList?: string[], isStop?: boolean, timer?: number, index?: number }`

在控制台中输出循环动画。

- `opts`: 可选参数对象，包含以下属性：
  - `loopList`: 动画字符列表，默认为 `['\\', '|', '/', '—', '—']`。
  - `isStop`: 是否停止循环，默认为 `false`。
  - `timer`: 动画切换的时间间隔（毫秒），默认为 `100`。
  - `index`: 当前动画字符的索引，默认为 `0`。

```javascript
logLoop(); // 启动默认循环动画
// 或者
logLoop({ loopList: ["-", "+", "-", "+"], timer: 200 }); // 启动自定义循环动画
```

#### animation 模块

##### 1. `class AnimateFrame`

提供帧动画的类。

- `start(duration?: number): void`: 启动帧动画，可选参数 `duration` 为帧数控制。
- `stop(): void`: 停止帧动画。

```javascript
const animateFrame = new AnimateFrame((timestamp) => {
  // 在此处更新动画状态
  console.log("AnimationFrame callback:", timestamp);
});

animateFrame.start(60); // 启动帧动画，每秒 60 帧
// 其他操作...
animateFrame.stop(); // 停止帧动画
```

##### 2. `quadraticBezier(_x: number, _y: number, t: number): [number, number]`

计算二次贝塞尔曲线上的点坐标。

- `_x`: 控制点 1 x 坐标。
- `_y`: 控制点 1 y 坐标。
- `t`: 时间参数，取值范围 [0, 1]。

```javascript
const result = quadraticBezier(0, 0, 1, 1, 0.5);
console.log(result);
// 输出: [0.75, 0.75]
```

##### 3. `cubicBezier(_x1: number, _y1: number, _x2: number, _y2: number, t: number): [number, number]`

计算三次贝塞尔曲线上的点坐标。

- `_x1`: 控制点 1 x 坐标。
- `_y1`: 控制点 1 y 坐标。
- `_x2`: 控制点 2 x 坐标。
- `_y2`: 控制点 2 y 坐标。
- `t`: 时间参数，取值范围 [0, 1]。

```javascript
const result = cubicBezier(0, 0, 0.5, 1, 0.5);
console.log(result);
// 输出: [0.375, 0.625]
```

##### 4. `factorial(n: number): number`

计算阶乘。

- `n`: 非负整数。

```javascript
const result = factorial(5);
console.log(result);
// 输出: 120
```

##### 5. `combination(n: number, k: number): number`

计算组合数。

- `n`: 总数。
- `k`: 选择的个数。

```javascript
const result = combination(5, 2);
console.log(result);
// 输出: 10
```

##### 6. `NBezier(points: number[][], t: number): [number, number]`

计算 N 次贝塞尔曲线上的点坐标。

- `points`: 控制点数组，每个点是一个二阶数组。
- `t`: 时间参数，取值范围 [0, 1]。

```javascript
const points = [
  [0, 0],
  [1, 1],
  [2, 0],
];
const result = NBezier(points, 0.5);
console.log(result);
// 输出: [1.5, 0.75]
```

#### event-message-center

https://gitee.com/DieHunter/message-center

#### task-queue-lib

https://gitee.com/DieHunter/task-queue

#### js-request-lib

https://gitee.com/DieHunter/js-request-lib

#### timer-manager-lib

https://gitee.com/DieHunter/timer-manager-lib

#### js-log-lib

https://gitee.com/DieHunter/js-log-lib
